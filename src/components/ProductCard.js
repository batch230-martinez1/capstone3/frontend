import { Card, Button, Modal, Row, Col, Container } from 'react-bootstrap';
import { useState, useContext } from 'react';
// import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import '../Card.css';

export default function ProductCard({ productProp }) {
  const { _id, name, description, price, stocks, category } = productProp;
	const { user } = useContext(UserContext);
  const [showModal, setShowModal] = useState(false);
  const [quantity, setQuantity] = useState(1);

  const handleBuyNowClick = (e) => {
    
    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: "POST",
      headers: {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify({
        productId: _id,
        quantity: quantity
    })
    })
    .then(res=> res.json())
    .then(data=>{
      console.log(data);
    if(data){Swal.fire({
      title: 'Purchase Complete',
      text: `Thank you for buying ${quantity} ${name}(s) for ${quantity * price}!`,
      icon: 'success'
    }
    );
  }
    else{
      Swal.fire({
        text: 'Something went wrong please try again later',
        icon: 'error'
      }
      );
    }
  })
  };

  const handleBuyClick = () => {
    setShowModal(true);
  };

  const handleQuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value, 10);
    setQuantity(newQuantity);
  };

  return (
    <>
  
     <Card className="productcard">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Card.Subtitle>Stocks:</Card.Subtitle>
        <Card.Text>{stocks}</Card.Text>
        <Button className="card-button  text-white" variant='success' onClick={handleBuyClick}>Order</Button>
      </Card.Body>
    </Card>

 
 
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{description}</p>
          <p>{price}</p>
          <label htmlFor="quantity-input">Quantity:</label>
          <input
            type="number"
            className="form-control"
            id="quantity-input"
            value={quantity}
            onChange={handleQuantityChange}
            min={1}
          />
        </Modal.Body>
				<Modal.Footer>
					
					{
						(user.id !== null) ?
						<>
							<Button variant="dark" onClick={handleBuyNowClick}>
								Buy Now
							</Button>
						</>
						:
						<Button as={Link} to="/login" variant="success" >Login to Purchase</Button>
					}
				</Modal.Footer>
					
      </Modal>
    </>
  );
}