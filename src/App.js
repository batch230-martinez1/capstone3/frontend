// remove: import logo from './logo.svg';
import './App.css';
import { Container } from 'react-bootstrap';
// import { Fragment } from 'react'; /* React Fragments allows us to return multiple elements*/ // s53 to comment
import { BrowserRouter as Router } from 'react-router-dom'; // s53 added
import { Route, Routes } from 'react-router-dom'; //s53 added

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout'; 
import Error from './pages/Error'; 
import Product from './pages/Products';
// import ProductCard from './components/ProductCard';
import AdminDashboard from './pages/AdminDashboard'
import Productsearch from './pages/Productsearch';
import Settings from './pages/Settings'; 



import { UserProvider } from './UserContext';
import { useState, useContext, useEffect } from 'react';

function App() {


  // 1 - create state
  const [user, setUser] = useState({
    // email: localStorage.getItem('email'),
    id: localStorage.getItem('id')
  })

  const unsetUser = () => ({
   id: localStorage.getItem('id')
  })
    // setUser({
    //   id: null,
    //   isAdmin: null
    // });
  

useEffect(()=>{
  fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
    }
})
.then(res => res.json())
.then(data => {
    console.log(data);
  if(typeof data.id !== undefined){  
      localStorage.setItem("userId",data._id)
      setUser({
      id: data._id,
      isAdmin: data.isAdmin
    })
    }
    else{
      setUser({
        id: null,
        isAdmin: null
      });
    }
  
  })

}, [])


  return (
    /* React Fragments allows us to return multiple elements*/

    // 2 - provide/share the state to other components
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route exact path="/" element={<Home />} />           
            <Route exact path="/productsearch" element={<Productsearch />} />
            <Route exact path="/products" element={<Product />} />
            <Route exact path="/admin" element={<AdminDashboard />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} /> 
            <Route exact path="/settings" element={<Settings />} />
            <Route exact path="*" element={<Error />} /> 
          </Routes>
        </Container>
      </ Router>
    </ UserProvider>
    
  );
}

export default App;




