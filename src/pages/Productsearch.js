import { Fragment } from 'react';
import { Navigate } from "react-router-dom";
import { useEffect, useState, useContext } from "react";
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Productsearch(){

	const { user } = useContext(UserContext);

	const [products, setProduct] = useState([]);
	const [searchTerm, setSearchTerm] = useState('');

	useEffect(() => {
		// Will retrieve filtered products
		fetch(`${process.env.REACT_APP_API_URL}/products/filter`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				name: searchTerm
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProduct(data.map(product => {
				return(
					<ProductCard key={product._id} productProp={product}/>
				);
			}));
		})
	}, [searchTerm]);

	const handleSearch = (e) => {
		setSearchTerm(e.target.value);
	};

	return(
		(user.isAdmin)
		?
			<Navigate to="/admin" />
		:
		<>
			<h1>Products</h1>
			<input type="text" placeholder="Search" onChange={handleSearch} />
			{products}
		</>
	)
}
