import {Navigate} from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext'  //s45

export default function Settings() {

	const { user, setUser } = useContext(UserContext);

	return (
		(user.email !== null)
		?
		<h3>Settings Page</h3>
		:
		<Navigate to="/login" />
	)
}
